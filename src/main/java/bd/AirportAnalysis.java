package bd;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.*;
import scala.Tuple2;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;

/**
 *
 Objective:
 ------------------------------------------
 Cada 5 segundos mirará en un directorio si hay ficheros nuevos. Fijando una ventana de 20 segundos
 y frecuencia de 10 segundos, el programa buscará los aeropuertos españoles y mostrará cuántos son
 small-airport, medium_airport, large_airport y heliport.

 csv file sample:
 ------------------------------------------
 ident,type,name,latitude_deg,longitude_deg,elevation_ft,continent,iso_country,iso_region,municipality,gps_code,iata_code,local_code
 00A,heliport,Total Rf Heliport,40.07080078,-74.93360138,11,NA,US,US-PA,Bensalem,00A,,00A

 Note: values of interest are in pos 1 (type) and 7 (iso_country)

 Output sample:
 -------------------------------------------
 Time: 1477844880000 ms
 -------------------------------------------

 -------------------------------------------
 Time: 1477844890000 ms
 -------------------------------------------
 (small_airport,11)
 (heliport,10)
 (medium_airport,8)

 -------------------------------------------
 Time: 1477844900000 ms
 -------------------------------------------
 (small_airport,22)
 (heliport,20)
 (medium_airport,16)

 -------------------------------------------
 Time: 1477844910000 ms
 -------------------------------------------
 (small_airport,22)
 (heliport,20)
 (medium_airport,16)

 -------------------------------------------
 Time: 1477844920000 ms
 -------------------------------------------
 (small_airport,11)
 (heliport,10)
 (medium_airport,8)

 -------------------------------------------
 Time: 1477844930000 ms
 -------------------------------------------
  */

public class AirportAnalysis
{
    public static void main( String[] args ) throws InterruptedException {
        if (args.length < 1) {
            System.err.println("Usage: AirportAnalysis");
            System.exit(1);
        }

        // 1. create the context with 5 seconds batch size
        SparkConf sparkConf = new SparkConf()
            .setMaster("local[2]")
            .setAppName("AirportAnalysis");

        JavaStreamingContext streamingContext = new JavaStreamingContext(sparkConf, Durations.seconds(5));

        // 2. bind source directory
        JavaDStream<String> inputStream = streamingContext.textFileStream(args[0]);

        // System.out.println("******************rawInput********************\n" );
        // inputStream.print();

        // 3. select relevant data (airport type and courtry) for all airports
        JavaPairDStream<String, String> airports = inputStream.mapToPair((PairFunction<String, String, String>) s -> {
            String [] split = s.split(",");
            String country = split[1];
            String airportType = split[7];
            //System.out.println("\n***************=> airport: " + country +  " : " + airportType);
            return new Tuple2<String, String>(country, airportType);
        });

        // System.out.println("******************airports********************\n" );
        // airports.print();

        // 4. filter airports from Spain
        JavaPairDStream<String, String> airportsES = airports.filter(
                (Function<Tuple2<String, String>, Boolean>) v1 -> v1._2.contentEquals("ES")
        );

        // map airports by type
        JavaPairDStream<String, Integer> airportTypes = airportsES.mapToPair(
                (PairFunction<Tuple2<String, String>, String, Integer>) typeCountryTuple2 -> new Tuple2<String, Integer>(typeCountryTuple2._1,1)
        );

         // reduce by airport type
         Function2<Integer, Integer, Integer> reduceFunction = (Function2<Integer, Integer, Integer>) (v1, v2) -> v1+v2;

        // reduce the last 20 seconds with a frequency of 10 seconds
        JavaPairDStream<String, Integer> windowedAirportTypesCount =
                airportTypes.reduceByKeyAndWindow(reduceFunction, Durations.seconds(20), Durations.seconds(10));

        windowedAirportTypesCount.print();
        streamingContext.start();
        streamingContext.awaitTermination();
    }
}
/*

spark-submit --class "bd.AirportAnalysis" --master local[2] target/airports-1.0-SNAPSHOT.jar data/

*/